library uncomplicated;

import 'dart:async';

typedef _VoidCallback = Function();
_VoidCallback _runOnce(_VoidCallback f) {
  bool ran = false;
  return () {
    if (ran) return;

    f();
    ran = true;
  };
}

typedef ExplicitListener<T> = Function(T data);

class ExplicitState<T> {
  final _controller = StreamController<T>.broadcast();

  T _value;

  T get value => _value;

  ExplicitState(T value) : _value = value;

  StreamSubscription<T> listen(ExplicitListener listener) =>
      _controller.stream.listen(listener);

  void add(T t) {
    _value = t;
    _controller.add(_value);
  }
}

typedef ComputedListener = Function(Snapshot snapshot);
typedef Builder<T> = FutureOr<T> Function();

class ComputedState<T> {
  final Builder<T> builder;
  final List<ComputedState> computedDependencies;
  final List<ExplicitState> explicitDependencies;

  ComputedState({
    required this.builder,
    this.computedDependencies = const [],
    this.explicitDependencies = const [],
  });

  Snapshot<T> _value = Snapshot.waiting();

  final _done = <ComputedState>{};
  late final _waiting = <ComputedState>{...computedDependencies};
  final _error = <ComputedState>{};

  late final oneTimeInitialization = _runOnce(() {
    for (final dependency in explicitDependencies) {
      dependency.listen((value) {
        rebuild();
      });
    }

    for (final dependency in computedDependencies) {
      dependency.listen(
        (event) async {
          if (_done.contains(dependency)) {
            _done.remove(dependency);
          } else if (_waiting.contains(dependency)) {
            _waiting.remove(dependency);
          } else if (_error.contains(dependency)) {
            _error.remove(dependency);
          } else {
            throw StateError(
                'Bug in uncomplicated. Dependency not found in either of _done, _waiting, _error');
          }

          final newState = event.match(
            data: (_) => _done,
            error: (_, __) => _error,
            waiting: () => _waiting,
          ) as Set<ComputedState>;
          newState.add(dependency);

          updateDependencies();
          _notifyListeners();
        },
      );
    }

    updateDependencies();
  });

  void updateDependencies() async {
    if (_error.isNotEmpty) {
      final e = _error.first.value;
      _value = Snapshot<T>.error(e._error, e._stacktrace);
      return;
    }

    if (_waiting.isNotEmpty) {
      _value = Snapshot.waiting();
      return;
    }

    rebuild();
  }

  void rebuild() {
    try {
      _value = Snapshot.waiting();
      _notifyListeners();
      final result = builder();
      if (result is! Future<T>) {
        _value = Snapshot.data(result);
        _notifyListeners();
        return;
      }

      result.then((value) {
        _value = Snapshot.data(value);
        _notifyListeners();
      }).catchError((error, stacktrace) {
        _value = Snapshot.error(error, stacktrace);
        _notifyListeners();
      });
    } catch (e, stacktrace) {
      _value = Snapshot.error(e, stacktrace);
      _notifyListeners();
    }
  }

  Snapshot<T> get value {
    oneTimeInitialization();
    return _value;
  }

  T get data => value.match(
        data: (data) => data,
        error: (error, stacktrace) =>
            throw StateError('data called on a BroadcastStream with error'),
        waiting: () => throw StateError(
            'data called on a BroadcastStream in waiting state'),
      );

  final _listeners = <ComputedListener>{};

  ComputedSubscription listen(ComputedListener listener) {
    oneTimeInitialization();
    _listeners.add(listener);
    return ComputedSubscription._(this, listener);
  }

  void _notifyListeners() {
    for (final listener in _listeners) {
      listener(value);
    }
  }

  void cancel(ComputedListener listener) {
    _listeners.remove(listener);
  }
}

enum _SnapshotState {
  waiting,
  error,
  data,
}

typedef DataCallback<T> = dynamic Function(T);
typedef ErrorCallback = dynamic Function(dynamic error, StackTrace? stacktrace);
typedef WaitingCallback = dynamic Function();

class Snapshot<T> {
  final _SnapshotState _state;
  final dynamic _error;
  final StackTrace? _stacktrace;

  final T? _data;

  Snapshot.waiting()
      : _state = _SnapshotState.waiting,
        _error = null,
        _stacktrace = null,
        _data = null;

  Snapshot.error(this._error, this._stacktrace)
      : _state = _SnapshotState.error,
        _data = null;

  Snapshot.data(this._data)
      : _state = _SnapshotState.data,
        _error = null,
        _stacktrace = null;

  dynamic match(
      {required DataCallback<T> data,
      required ErrorCallback error,
      required WaitingCallback waiting}) {
    if (_state == _SnapshotState.waiting) {
      return waiting();
    }

    if (_state == _SnapshotState.error) {
      return error(_error, _stacktrace);
    }

    return data(_data as T);
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Snapshot &&
          runtimeType == other.runtimeType &&
          _state == other._state &&
          _error == other._error &&
          _stacktrace == other._stacktrace &&
          _data == other._data;

  @override
  int get hashCode =>
      _state.hashCode ^ _error.hashCode ^ _stacktrace.hashCode ^ _data.hashCode;

  @override
  String toString() {
    if (_state == _SnapshotState.waiting) {
      return '_Snapshot[state: waiting]';
    }

    if (_state == _SnapshotState.error) {
      return '_Snapshot[state: error, $_error $_stacktrace}]';
    }

    return '_Snapshot[state: data, $_data]';
  }
}

class ComputedSubscription {
  final ComputedState _state;
  final ComputedListener _listener;

  ComputedSubscription._(this._state, this._listener);

  void cancel() {
    _state.cancel(_listener);
  }
}
