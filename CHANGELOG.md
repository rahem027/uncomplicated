## 0.0.6
Fixed missing await in Readme.md

## 0.0.5
Updated repository url

## 0.0.4
Moved uncomplicated_flutter to a different dependency

## 0.0.3
Removed flutter dependency

## 0.0.2
* No code change. Trying to get pub.dev to recognize license 

## 0.0.1

* Added ExplicitState and ComputedState 
