<!--
This README describes the package. If you publish this package to pub.dev,
this README's contents appear on the landing page for your package.

For information about how to write a good package README, see the guide for
[writing package pages](https://dart.dev/guides/libraries/writing-package-pages).

For general information about developing packages, see the Dart guide for
[creating packages](https://dart.dev/guides/libraries/create-library-packages)
and the Flutter guide for
[developing packages and plugins](https://flutter.dev/developing-packages).
-->

# Uncomplicated
A very simple state management library with very small codebase
and no flutter dependencies

## Features
1. Easy to use
2. 0 dependencies. Depends only on Dart SDK
3. Entire library is some 300 SLOC. Less code means less bugs

## Getting started
```shell
dart pub add uncomplicated
```
or 
```shell
flutter pub add uncomplicated
```

## Usage

There are 2 main classes in this library - 
1. `ExplicitState` and
2. `ComputableState`

`ExplicitState` is a very thin wrapper on top of Stream.broadcast.
```dart
final counter = ExplicitState(0);

// value returns current state
print(counter.value); // prints 0
final subscription = counter.listen((e) => print(i));
counter.add(1); // prints 1 because of listener on above line
subscription.cancel(); // optionally cancel the subscription.
// Note: there is no way to close this stream intentionally.
```

**Note:** 
`Stream.broadcast` does not synchronously call its listeners. It schedules
a microtask. Which means:
```dart
final counter = ExplicitState(0);
counter.add(1);
print(counter.value); // prints 0 till micro events are processed
```

This is not usually a problem.

`ComputedState` This is where the magic happens. We specify the dependencies
like d depends on a, b and c. So whenever these change, we call the builder
again to obtain the new state
```dart
final a = ExplicitState(2);
final b = ExplicitState(3);
final c = ComputedState(builder: () => 4);
final d = ComputedState(
  // for computed state, .value returns a snapshot which is equivalent
  // to AsyncSnapshot i.e. it can be waiting, error, or have data
  // inside builder, we should use data as ComputedState handles waiting
  // and error of dependencies automatically
  builder: () => a.value + b.value * c.data,
  explicitDependencies: [a, b],
  computedDependencies: [c]
);

// note the dependencies are not registered till you use .listen
// or .value for the first time
print(d.data) // 2 + 3 * 4 = 2 + 12 = 14;
```

## API calls
`ComputeState` understands Future. `ComputeState.value` is a Snapshot object
which is a monad representing 
1. waiting for Future to complete
2. future completed with error
3. future completed with data

```dart
final state = ComputedState(builder: () => Future.delayed(Duration(seconds: 1), () => 1));
print(state.value); // prints _Snapshot[state: waiting]
await Future.delayed(Duration(seconds: 1);
print(state.value); // prints _Snapshot[state: data, 1]

final state2 = ComputedState(builder: () => Future.delayed(Duration(seconds: 1), () => throw StateError('')));
print(state2.value); // prints _Snapshot[state: waiting]
await Future.delayed(Duration(seconds: 1);
print(state2.value); // prints _Snapshot[state: error, ...]
```

## Accessing data from Snapshot
The only way to access data from Snapshot is: 

```dart
state.value.match(
  data: (data) => print(data),
  error: (error, stacktrace) { print(error); print(stacktrace); },
  waiting: () => print('waiting'),
);
```

This ensures you handle all the conditions and if you don't, the code
is very explicit that we dont care about these conditions

If you are damn sure you got back valid data, you can use `state.data`.
`state.data` is a wrapper over match which throws StateError if you call
it on an object which is in error or waiting state. 

**Only use `state.data` inside the builder argument to `ComputedState`**