// ignore_for_file: prefer_void_to_null
/// We need this for the following line. Without the above
/// ignore, the line warns us not to use Null. But if we
/// don't then expect breaks because of the way type system
/// works in dart
/// `expect(stream1.value, equals(Snapshot<Null>.waiting()));`

import 'package:test/test.dart';

import 'package:uncomplicated/uncomplicated.dart';

void main() {
  group('Explicit state tests', () {
    test('Basic tests', () async {
      final stream = ExplicitState(0);

      expect(stream.value, equals(0));

      /// Ensure stream can have multiple listeners
      int i = 0;
      stream.listen((e) => i = e);

      int j = 0;
      stream.listen((e) => j = e);

      stream.add(1);
      /// ExplicitState is a lightweight wrapper over Stream.broadcast
      /// which calls listener in a microtask. So we need to wait some
      /// time
      await Future.delayed(const Duration(milliseconds: 1));
      expect(i, equals(1));
      expect(j, equals(1));
    });
  });
  group('Broadcast Stream tests', () {
    test('input function is only called when .value or .listen is called',
        () async {
      int i = 0;
      final stream1 = ComputedState(builder: () => ++i);
      expect(i, equals(0));

      expect(stream1.data, equals(1));
      expect(i, equals(1));

      int j = 0;
      final stream2 = ComputedState(builder: () => ++j);
      expect(j, equals(0));

      stream2.listen((_) {});
      expect(j, equals(1));

      expect(stream2.data, equals(1));
    });

    test(
        'if dependencies are specified, change in dependencies retrigger input function',
        () async {
      final stream1 = ExplicitState(0);

      int j = 0;
      final stream2 =
          ComputedState(builder: () => ++j, explicitDependencies: [stream1]);

      int k = 0;
      final stream3 =
          ComputedState(builder: () => ++k, explicitDependencies: [stream1]);

      stream1.add(1);

      /// we have not begun listen or asked for the value. So the function
      /// is not called and dependencies are not set up
      expect(j, 0);
      expect(k, 0);

      /// now we call .value which triggers setting up dependency and calling
      /// input function the first time it runs
      expect(stream1.value, equals(1));
      expect(stream2.data, equals(1));
      await Future.delayed(const Duration(milliseconds: 1));
      expect(j, 1);

      /// .listen sets up dependency and calls input function the first time it runs
      stream3.listen((_) {});
      expect(stream3.data, equals(1));
      expect(k, 1);
    });

    test('multiple dependencies are supported', () async {
      final stream1 = ExplicitState(21);
      final stream2 = ExplicitState(42);
      final stream3 = ComputedState(
        builder: () => stream1.value + stream2.value,
        explicitDependencies: [stream1, stream2],
      );

      expect(stream3.data, equals(63));
      stream1.add(10);
      await Future.delayed(const Duration(milliseconds: 1));
      expect(stream3.data, equals(52));
      stream2.add(20);
      await Future.delayed(const Duration(milliseconds: 1));
      expect(stream3.data, equals(30));
    });

    test(
        'if input function returns a future, value is waiting till future is completed',
        () async {
      final intStream1 = ExplicitState(21);
      final stream1 = ComputedState(
          builder: () => Future.delayed(const Duration(seconds: 1), () => intStream1.value),
          explicitDependencies: [intStream1]);
      final intStream2 = ExplicitState(42);
      final stream2 = ComputedState(
          builder: () => Future.delayed(const Duration(seconds: 1), () => intStream2.value),
          explicitDependencies: [intStream2]);
      final stream3 = ComputedState(
          builder: () => stream1.data + stream2.data,
          computedDependencies: [stream1, stream2]);

      expect(stream1.value, equals(Snapshot<int>.waiting()));
      expect(stream2.value, equals(Snapshot<int>.waiting()));
      expect(stream3.value, equals(Snapshot<int>.waiting()));

      await Future.delayed(const Duration(seconds: 1));
      expect(stream1.value, equals(Snapshot<int>.data(21)));
      expect(stream2.value, equals(Snapshot<int>.data(42)));
      expect(stream3.value, equals(Snapshot<int>.data(63)));

      /// re-trigger stream1
      intStream1.add(10);
      await Future.delayed(const Duration(milliseconds: 1));
      expect(stream1.value, equals(Snapshot<int>.waiting()));
      expect(stream2.value, equals(Snapshot<int>.data(42)));
      expect(stream3.value, equals(Snapshot<int>.waiting()));
      await Future.delayed(const Duration(seconds: 1));
      expect(stream1.value, equals(Snapshot<int>.data(10)));
      expect(stream2.value, equals(Snapshot<int>.data(42)));
      expect(stream3.value, equals(Snapshot<int>.data(52)));

      /// re-trigger stream2
      intStream2.add(30);
      await Future.delayed(const Duration(milliseconds: 1));
      expect(stream1.value, equals(Snapshot<int>.data(10)));
      expect(stream2.value, equals(Snapshot<int>.waiting()));
      expect(stream3.value, equals(Snapshot<int>.waiting()));
      await Future.delayed(const Duration(seconds: 1));
      expect(stream1.value, equals(Snapshot<int>.data(10)));
      expect(stream2.value, equals(Snapshot<int>.data(30)));
      expect(stream3.value, equals(Snapshot<int>.data(40)));
    });

    test(
        'If a dependency throws an error, we change the state to the first error of the dependency',
        () async {
      final stream1 = ComputedState<Null>(
        builder: () => Future.delayed(
            const Duration(seconds: 1), () => throw UnimplementedError()),
      );

      final stream2 = ComputedState<Null>(
        builder: () =>
            Future.delayed(const Duration(seconds: 2), () => throw StateError('')),
      );

      final stream3 = ComputedState<Null>(
          builder: () {}, computedDependencies: [stream1, stream2]);

      expect(stream1.value, equals(Snapshot<Null>.waiting()));
      expect(stream2.value, equals(Snapshot<Null>.waiting()));
      expect(stream3.value, equals(Snapshot<Null>.waiting()));
      await Future.delayed(const Duration(seconds: 1));

      void unimplementedError(error, stacktrace) {
        expect(error, isA<UnimplementedError>());
        expect(error, isNotNull);
      }

      stream1.value.match(
        data: neverCalled,
        waiting: neverCalled,
        error: unimplementedError,
      );

      stream3.value.match(
        data: neverCalled,
        waiting: neverCalled,
        error: unimplementedError,
      );

      await Future.delayed(const Duration(seconds: 1));

      stream3.value.match(
        data: neverCalled,
        waiting: neverCalled,
        error: unimplementedError,
      );
    });
    
    test('cancelled subscription should not receive events', () async {
      final stream1 = ExplicitState(0);
      int i = 0;
      stream1.listen((_) => ++i);
      int j = 0;
      final subscription = stream1.listen((_) => ++j);

      stream1.add(1);
      await Future.delayed(const Duration(milliseconds: 1));
      expect(i, equals(1));
      expect(j, equals(1));

      subscription.cancel();

      stream1.add(2);
      await Future.delayed(const Duration(milliseconds: 1));
      expect(i, equals(2));
      expect(j, equals(1));
    });
  });
}
